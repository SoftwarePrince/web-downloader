var urlInfoArr = [[], []];
var stor = chrome.storage.local;
stor.set({ status: "Stopped", btn_status: "Start" });
stor.set({ toVisit: [], Visited: [] }, () => {
    console.log("cleared")
});

chrome.downloads.onDeterminingFilename.addListener(function (item, suggest) {
    console.log("prev name", item.filename, item);
    var saveAs = item.finalUrl;
    var suggested = false;

    if (saveAs.indexOf("blob:") == 0) {
        urlInfoArr[0].map((item, index) => {
            if (item == saveAs) {
                saveAs = urlInfoArr[1][index];
                console.log(item, saveAs, index, urlInfoArr[index], urlInfoArr[1], urlInfoArr);
            }
        });
    }

    console.log("save as " + saveAs);

    saveAs = saveAs.replace("http://", "").replace("https://", "").replace(".php", ".html").replace(".asp", ".html").replace(":", "").replace("//", "");
    console.log("removed http ", saveAs);
    clearPath();
    function clearPath() {
        // console.log("clearPath");
        if (!saveAs[0].match(/\w/i)) {
            console.log("clearing ", saveAs);
            saveAs = saveAs.substring(1);
            clearPath();
        }
    }

    //This line 34 is creating delay, code on line 54 executes first and this later
    // so make that wait few milliseconds or create some faster communication 

    chrome.storage.local.get(['win_status', 'win_l'], (ws) => {
        if (ws.win_status === 200) {
            if (!(saveAs.indexOf(".html") != -1 || saveAs.indexOf(".asp") != -1 || saveAs.indexOf(".js") != -1 || saveAs.indexOf(".css") != -1 || saveAs.indexOf(".jsx") != -1 || saveAs.indexOf(".php") != -1 || saveAs.indexOf(".htm") != -1 || saveAs.indexOf(".png") != -1 || saveAs.indexOf(".jpg") != -1 || saveAs.indexOf(".jpeg") != -1 || saveAs.indexOf(".svg") != -1 || saveAs.indexOf(".gif") != -1 || saveAs.indexOf(".ico") != -1)) {
                console.log("extension not ", saveAs);
                if (saveAs[saveAs.length - 1] == "/") {
                    saveAs += "index.html"
                } else {
                    saveAs += ".html";

                }
            }

        }

        sugest();
    });

    function sugest() {
        if (suggested) return;
        suggested = true;
        console.log("SAVing AS ", saveAs, "\n");
        suggest({ filename: saveAs, conflictAction: 'overwrite' });
    }
    return true;

});

var msgNum = 0;
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    msgNum++;
    if (request.do == "download") {

        if (request.content) {
            var url = request.content;
            urlInfoArr[0].push(url);
            urlInfoArr[1].push(request.on);
        }
        else
            var url = request.on;
        console.log("strting download", url, request);

        let obj = {
            url: url,
            conflictAction: "overwrite",
            saveAs: false
        };

        setTimeout((obj) => {
            console.log(obj);
            chrome.downloads.download(obj);

        }, msgNum + 10, obj);

        sendResponse({

        });

    }

    try {

        if (request.msg == "next url") {
            setTimeout(() => {
                console.log('script exe');
                chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
                    chrome.tabs.executeScript(tabs[0].id, {
                        file: 'content_script.js'
                    });
                });
            }, 5000);
        }

        if (request.exe == "content_script") {
            chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
                chrome.tabs.executeScript(tabs[0].id, {
                    file: 'content_script.js'
                });
            });
        }

        if (request.exe == "no_script") {
            chrome.runtime.sendMessage({ msg: "domain_blacklisted" });
        }
    } catch (err) {
        console.log("errn1", err);
    }

});

/*var CONTEXT_MENU_ID = "change_link_786";
var CONTEXT_MENU_ID_1 = "change_image_786";*/
var CONTEXT_MENU_ID_2 = "website_downloader_010";

/*chrome.contextMenus.create({

    title: "Select Link",
    contexts:["link"],
    id: CONTEXT_MENU_ID
});

chrome.contextMenus.create({

    title: "Select Image",
    contexts:["image"],
    id: CONTEXT_MENU_ID_1
});

chrome.contextMenus.create({
    title: "Pause Web Downloader",
    contexts: ["page"],
    id: CONTEXT_MENU_ID_2
});

chrome.contextMenus.onClicked.addListener(click_function);
*/


function click_function(info, tab) {
    //console.log(info.linkUrl);
    /*if (info.menuItemId == CONTEXT_MENU_ID) {
            var linkurl = info.linkUrl;
            var link = linkurl.match(/[hpt]{4}s?:\/\/[^/]+/g);
        chrome.storage.sync.set({link: link}, function() {
              console.log('Value is set to ' + linkurl);
            });
        chrome.browserAction.setPopup({popup: "link.html"});
    }
    console.log(info.srcUrl);
    if (info.menuItemId == CONTEXT_MENU_ID_1) {
        var imageurl = info.srcUrl;
        var image = imageurl.match(/[a-zA-Z0-9-_]{0,100}\.[pjgnvifes]{3,4}/g);
        chrome.storage.sync.set({image_url: image}, function() {
              console.log('Value is set to ' + image_url);
            });
        chrome.browserAction.setPopup({popup: "image.html"});
    }*/
    if (info.menuItemId == CONTEXT_MENU_ID_2) {
        stor.set({ status: "Paused", btn_status: "Start" });
    }
}