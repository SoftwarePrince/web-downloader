var z = 0;
var zz = 0;
var new_url = "";
var box = chrome.storage.local;
var msg = chrome.runtime;
var my_random;
console.log("location: ", location.href);

var tld = get_top_domain();

var sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

function matchwild(str, rule) {
  var escapeRegex = (str) => str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
  return new RegExp("^" + rule.split("*").map(escapeRegex).join(".*") + "$").test(str);
}

function get_top_domain() {
  var i, h,
    weird_cookie = 'weird_get_top_level_domain=cookie',
    hostname = document.location.hostname.split('.');
  for (i = hostname.length - 1; i >= 0; i--) {
    h = hostname.slice(i).join('.');
    document.cookie = weird_cookie + ';domain=.' + h + ';';
    if (document.cookie.indexOf(weird_cookie) > -1) {
      document.cookie = weird_cookie.split('=')[0] + '=;domain=.' + h + ';expires=Thu, 01 Jan 1970 00:00:01 GMT;';
      return h;
    }
  }
}

function getArrayofLinks(select, attribute) {
  var x = document.querySelectorAll(select);
  var myarray = [];
  for (var i = 0; i < x.length; i++) {
    var cleanlink = (attribute == "src") ? x[i].src : x[i].href;
    if (cleanlink) {
      myarray.push(cleanlink);
      //console.log(cleanlink);
    }
  }
  return myarray;
}

function downloadEach(file) {
  file.map((item, index) => {
    console.log(item, item.includes(location.hostname));
    // if (item.includes(location.hostname)) {
      msg.sendMessage({
        do: "download", on: item
      }, function (response) {
        console.log("resp from bg ", item, "[href='" + item + "']");
        console.log(document.querySelector("[href='" + item + "']"));
        console.log(document.querySelector("[src='" + item + "']"));
        if (index === file.length - 1 || document.querySelectorAll('img').length == 0 || document.querySelectorAll('link').length == 0 || document.querySelectorAll('script').length == 0) {
          console.log(file.length);
          z += 1;
          console.log(z);
          if (z === 3) {
            // box.get(['Visited', 'toVisit'], (st) => {
            //   let win_location = window.location.href;
            //   if (st.Visited === undefined) {
            //     box.set({ Visited: [] });
            //     st.Visited = [];
            //   }
            //   if (st.toVisit === undefined) {
            //     box.set({ toVisit: [] });
            //     st.Visited = [];
            //   }
            //   if (st.Visited.indexOf(win_location) === -1) {
            //     st.Visited.push(win_location);
            //     st.toVisit.shift();
            //   } else {
            //     console.log();
            //   }
            //   box.set({ Visited: st.Visited, toVisit: st.toVisit });
            //   startAgain();
            // })
          }
        }
      });
    // }
  });
  if (file) {
  zz+=1;
  console.log(zz);
  if(zz === 3){
    console.log("finalize");
    finalize();
  }
}
}

function download_CSS_JS() {
  let css = getArrayofLinks("link", "href");
  downloadEach(css);

  let js = getArrayofLinks("script", "src");
  downloadEach(js);

  let img = getArrayofLinks("img", "src");
  downloadEach(img);

}

box.get(['from_start', 'toVisit'], (fs) => {
  if (fs.from_start == "yes") {
    if (fs.toVisit === undefined) {
      box.set({ toVisit: [] });
      fs.toVisit = [];
    }
    fs.toVisit.push(window.location.href);
    box.set({ from_start: "no", toVisit: fs.toVisit });
  }
})

fetch(window.location.href).then((response) => {
  if (response.status === 200) {
    console.log(response.status);
    download_CSS_JS();
    updateToVisit();
    box.set({ win_status: 200, win_l: window.location.href });
  }
  if (response.status === 404) {
    console.log(response.status);

    msg.sendMessage({ msg: "next url" });
    box.set({ win_status: 404, win_l: window.location.href });
    box.get(['Visited', 'toVisit'], (st) => {
      let win_location = window.location.href;
      if (st.Visited === undefined) {
        box.set({ Visited: [] });
        st.Visited = [];
      }
      if (st.toVisit === undefined) {
        box.set({ toVisit: [] });
        st.Visited = [];
      }
      if (st.Visited.indexOf(win_location) === -1) {
        st.Visited.push(win_location);
        st.toVisit.shift();
      } else {
        console.log();
      }
      box.set({ Visited: st.Visited, toVisit: st.toVisit });
      startAgain();
    })
  }
});


async function finalize() {
  await sleep(4000)
  updateAllLinks();
  downloadThisPage();
}


var my_random;


function updateToVisit() {
  let hrefs = getArrayofLinks("a", "href");
  box.get(['toVisit'], function (result) {
    console.log('res', result);
    if (typeof result.toVisit === "undefined") {
      result.toVisit = [];
    }
    let win_loc = window.location.origin;
    hrefs.map(item => {
      console.log(item);
      let check = item.match(/https?\:\/\/[a-z:0-9._-]+/gi);
      console.log(check);
      console.log(win_loc);
      if (check != null) {
        if (check[0] == win_loc) {
          let check_2 = item.match(/^([a-z0-9:/_.-]+)/gi);
          if (check_2 != null) {
            console.log(check_2[0]);
            (result.toVisit.indexOf(check_2[0]) === -1) ?
              result.toVisit.push(check_2[0]) : console.log();
          }
        }
      }
    });
    box.set({ toVisit: result.toVisit });
    console.log(result.toVisit);
  });

}
function addRelativePath(httpPath, realPath) {
  //get url of the tab in brrowser
  //remove realPath from it
  //remove slashes from 0 and last index
  //use for loop to add that much ../ in front of realPath

  var tabPath = window.location.href;
  console.log("tab", tabPath, realPath);
  tabPath = tabPath.toLowerCase();
  // tabPath = tabPath.replace("http:/").replace("https:/");
  var realPath = tabPath;
  clearPath();

  function clearPath() {
    console.log("clearPath1", realPath);
    if (!realPath[0].match(/\w/i)) {
      realPath = realPath.substring(1);
      clearPath();
    }
    if (!realPath[realPath.length - 1].match(/\w/i)) {
      realPath = realPath.substring(0, realPath.length - 1);;
      clearPath();
    }
  }

  for (i = 0; i < realPath.split("/").length; i++) {
    realPath = "../" + realPath;
  }
  console.log("returning", realPath);
  return realPath;
}

function updateLinks(elem, attrib) {
  var realPath = elem.getAttribute(attrib);
  console.log("updateLinks ", realPath, attrib, elem);
  if (!realPath) return;
  realPath = realPath.toLowerCase()
  var httpPath = elem.src;
  if (!httpPath) httpPath = elem.href;
  httpPath = httpPath.toLowerCase();

  clearPath();
  function clearPath() {
    return;
    console.log("clearPath2", realPath);
    if (realPath.length == 0) return;

    if (!realPath[0].match(/\w/i)) {
      console.log("removing first ", realPath);
      realPath = realPath.substring(1);
      clearPath();
    }

    if (realPath.length == 0) return;

    if (!realPath[realPath.length - 1].match(/\w/i)) {
      console.log("removing last ", realPath);
      realPath = realPath.substring(0, realPath.length - 1);;
      clearPath();
    }
  }


  if (!httpPath.includes(tld)) {
    // console.log("returned addRelativePath ", addRelativePath(httpPath, realPath));
    //realPath=addRelativePath(httpPath, realPath);
  }

  realPath.replace(".php", ".html").replace(".asp", ".html");
  realPath = realPath.replace("http:/", "").replace("https:/", "").replace(":", "").replace("//", "/");
  var saveAs = realPath;
  if (!(saveAs.indexOf(".html") != -1 || saveAs.indexOf(".asp") != -1 || saveAs.indexOf(".js") != -1 || saveAs.indexOf(".css") != -1 || saveAs.indexOf(".jsx") != -1 || saveAs.indexOf(".php") != -1 || saveAs.indexOf(".htm") != -1 || saveAs.indexOf(".png") != -1 || saveAs.indexOf(".jpg") != -1 || saveAs.indexOf(".jpeg") != -1 || saveAs.indexOf(".svg") != -1 || saveAs.indexOf(".gif") != -1 || saveAs.indexOf(".ico") != -1)) {
    console.log("extension not ", saveAs);
    if (saveAs[saveAs.length - 1] == "/") {
      // saveAs += "index.html"
    } else {
      saveAs += ".html";

    }
  }
  realPath = saveAs;
  elem.setAttribute(attrib, realPath);
  console.log("broken links fixed", realPath, elem);
}

function loopUpdateLinks(elemArray, attrib) {
  for (var i = 0; i < elemArray.length; i++) {
    updateLinks(elemArray[i], attrib);
  }
}

function updateAllLinks() {
  var x = document.querySelectorAll("link");
  loopUpdateLinks(x, "href");
  x = document.querySelectorAll("script");
  loopUpdateLinks(x, "src");
  x = document.querySelectorAll("img");
  loopUpdateLinks(x, "src");
  x = document.querySelectorAll("a");
  loopUpdateLinks(x, "href");

}

function downloadThisPage() {

  console.log("sending msg1");

  let markup = document.documentElement.outerHTML;
  var blob = new Blob([markup], { type: "html/plain" });
  var url = URL.createObjectURL(blob);
  console.log("sending msg");

  msg.sendMessage({
    do: "download", content: url, on: location.href
  }, function (response) {

  });

  box.get(['Visited', 'toVisit'], (st) => {
    let win_location = window.location.href;
    if (st.Visited === undefined) {
      box.set({ Visited: [] });
      st.Visited = [];
    }
    if (st.toVisit === undefined) {
      box.set({ toVisit: [] });
      st.Visited = [];
    }
    if (st.Visited.indexOf(win_location) === -1) {
      st.Visited.push(win_location);
      st.toVisit.shift();
    } else {
      console.log();
    }
    box.set({ Visited: st.Visited, toVisit: st.toVisit });
    startAgain();
  })



}

// chrome.storage.sync.get(['toVisit'], (get)=>{
//   let Visitable = get.toVisit;

//   Visitable.map((item)=>{

//   })
// })

function startAgain() {
  console.log("start again started");
  box.get(['toVisit', 'Visited', 'white_list', 'black_list'], (data) => {
    if (data.Visited === undefined) {
      box.set({ Visited: [] });
      data.Visited = [];
    }
    let toVisit = data.toVisit;
    let Visited = data.Visited;
    checking();
    function checking() {
      if (data.white_list == "") {
        if (data.black_list == "") {
          if (Visited.indexOf(toVisit[0]) === -1) {
            console.log(toVisit[0]);
            my_rand(toVisit[0]);
          } else {
            toVisit.shift();
            box.set({ toVisit: toVisit });
            checking();
          }
        } else {
          if (toVisit[0].match(data.black_list, "g") === null) {
            if (Visited.indexOf(toVisit[0]) === -1) {
              console.log(toVisit[0]);
              my_rand(toVisit[0]);
            } else {
              toVisit.shift();
              box.set({ toVisit: toVisit });
              checking();
            }
          } else {
            toVisit.shift();
            box.set({ toVisit: toVisit });
            checking();
          }
        }
      } else {
        let w_listing = toVisit[0].match(data.white_list, "g");
        if (w_listing !== null) {
          if (Visited.indexOf(toVisit[0]) === -1) {
            console.log(toVisit[0]);

            my_rand(toVisit[0]);
          } else {
            toVisit.shift();
            box.set({ toVisit: toVisit });
            checking();
          }
        } else {
          toVisit.shift();
          box.set({ toVisit: toVisit });
          checking();
        }
      }

    }

    // for(let i=0; i<toVisit.length; i++){
    //   console.log("1st loop started");
    //   console.log(Visited.length);
    //   let ch_visit = Visited.find((ch)=>{
    //      if(ch == Visited[i]){
    //        return true;
    //      }
    //   });
    //   console.log(ch_visit);
    //   if(ch_visit === undefined){
    //     new_url = toVisit[i];
    //     break;
    //   }
    //   new_url = "";
    // }

    //   if(new_url != ""){
    //       my_rand();

    // }
  });
}

function my_rand(url_v) {
  console.log("my_rand() ", url_v);
  if (!url_v) return;

  box.get(['min_delay', 'max_delay'], (time) => {
    console.log(time.min_delay + " " + time.max_delay);
    if (time.max_delay < 10) {
      let rand_1 = parseInt(Math.random() * 10);
      console.log(rand_1);
      if (rand_1 > time.min_delay && rand_1 < time.max_delay) {
        my_random = rand_1;
        if (my_random != undefined) {
          setTimeout(() => {
            box.get(['status'], (get) => {
              if (get.status == "Started") {
                setTimeout(() => {
                  window.close();
                }, 5000);
                window.open(url_v);
                msg.sendMessage({ msg: "next url" });
                console.log("closing 1");
              }
            })
          }, my_random * 1000);
        }
        console.log(my_random);
      } else {
        my_rand(url_v)
      }

    } else if (time.max_delay > 9 && time.max_delay < 60) {
      let rand_2 = parseInt(Math.random() * 100);
      console.log(rand_2);
      if (rand_2 > time.min_delay && rand_2 < time.max_delay) {
        my_random = rand_2;
        if (my_random != undefined) {
          setTimeout(() => {
            box.get(['status'], (get) => {
              if (get.status == "Started") {
                setTimeout(() => {
                  window.close();
                }, 5000);
                window.open(url_v);
                msg.sendMessage({ msg: "next url" });
                console.log("closing 2");

              }
            })
          }, my_random * 1000);
        }
        console.log(my_random);
      } else {
        my_rand(url_v)
      }
    }
  });
}


