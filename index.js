var stor = chrome.storage.local;
var new_url;
var msg = chrome.runtime;
stor.get(['btn_status'], (get) => {
    if (get.btn_status === undefined) {
        stor.set({ btn_status: "Start" });
    }
    console.log(get.btn_status);
    if (get.btn_status == "Start" || get.btn_status === undefined) {
        document.querySelector('#start').innerHTML = "Start";
    } else if (get.btn_status == "Pause") {
        document.querySelector('#start').innerHTML = "Pause";
    }
})

stor.get(['min_delay','max_delay'], (get)=>{
    if(get.min_delay !== undefined && get.max_delay !== undefined){
        document.querySelector('#min_delay').value = get.min_delay;
        document.querySelector('#max_delay').value = get.max_delay;
    }
})

stor.get(['white_list','black_list'], (get)=>{
    if(get.white_list !== undefined){
        document.querySelector('#white_list').value = get.white_list.toString();
    }
    if(get.black_list !== undefined){
        document.querySelector('#black_list').value = get.black_list.toString();
    }
})

document.getElementById("start").addEventListener("click", function () {
    var start_btn_text = document.querySelector('#start');
    if (start_btn_text.innerHTML == "Start") {
        var min_delay = document.querySelector('#min_delay').value;
        var max_delay = document.querySelector('#max_delay').value;
        if (min_delay != "" && max_delay != "") {
            stor.set({ min_delay: min_delay, max_delay: max_delay });
            var white_list = document.querySelector('#white_list').value;
            var black_list = document.querySelector('#black_list').value;
            
            stor.set({white_list: white_list, black_list: black_list});
            stor.get(['toVisit', 'Visited','white_list','black_list'], (data) => {
                if (data.Visited === undefined) {
                    stor.set({ Visited: [] });
                    data.Visited = [];
                }
                let toVisit = data.toVisit;
                let Visited = data.Visited;
                if (toVisit.length > 0) {
                    if(data.white_list == ""){
                        if(data.black_list == ""){
                            checking();
                            function checking () {
                            if(Visited.indexOf(toVisit[0]) === -1){
                                console.log(toVisit[0]);
                                window.open(toVisit[0]);
                            msg.sendMessage({ msg: "next url" });
                            stor.set({ status: "Started", btn_status: "Pause" });
                            } else {
                                toVisit.shift();
                                stor.set({toVisit: toVisit});
                                checking();
                                } 
                            }
                        } else {
                            checking_3();
                            function checking_3 () {
                                console.log(toVisit[0].match(data.black_list, "g"));
                            if(toVisit[0].match(data.black_list, "g") === null){
                                if(Visited.indexOf(toVisit[0]) === -1){
                                    console.log(toVisit[0]);
                                    window.open(toVisit[0]);
                                msg.sendMessage({ msg: "next url" });
                                stor.set({ status: "Started", btn_status: "Pause" });
                                } else {
                                    toVisit.shift();
                                    stor.set({toVisit: toVisit});
                                    checking_3();
                                    }
                            } else {
                                toVisit.shift();
                                    stor.set({toVisit: toVisit});
                                    checking_3();
                            } 
                            } 
                        }
                        
                    } else {
                        checking_2();
                            function checking_2 () {
                                let w_listing = toVisit[0].match(data.white_list, "g");
                            if(w_listing != null){
                                if(Visited.indexOf(toVisit[0]) === -1){
                                    console.log(w_listing[0]);
                                    window.open(toVisit[0]);
                                msg.sendMessage({ msg: "next url" });
                                stor.set({ status: "Started", btn_status: "Pause" });
                                } else {
                                    toVisit.shift();
                                    stor.set({toVisit: toVisit});
                                    checking_2();
                                    } 
                            } else {
                                toVisit.shift();
                                    stor.set({toVisit: toVisit});
                                    checking_2();
                            }
                                
                                }
                    }
    
                } else {
                    chrome.tabs.executeScript(null, {
                        file: 'side_script.js'
                    }, () => {
                        stor.set({ status: "Started", btn_status: "Pause", from_start: "yes" });
                    });
                }

            })
            start_btn_text.innerHTML = "Pause";
        } else {
            document.querySelector('#show').innerHTML = "Please Enter Min/Max Delays";
            setTimeout(() => {
                document.querySelector('#show').innerHTML = "";
            }, 3000);
        }
    } else if (start_btn_text.innerHTML == "Pause") {
        start_btn_text.innerHTML = "Start";
        stor.set({ status: "Paused", btn_status: "Start" });
    }


});


document.getElementById("clear").addEventListener("click", function () {
    stor.set({ status: "Stopped", btn_status: "Start" });
    stor.set({toVisit: [], Visited: []}, ()=>{
        console.log("cleared")
       });
    document.querySelector('#start').innerHTML = "Start";
    document.querySelector('#show').innerHTML = "Data has been Cleared!";
            setTimeout(() => {
                document.querySelector('#show').innerHTML = "";
            }, 3000);
});

stor.get(['Visited', 'toVisit','white_list'], (v) => {
    let doma = 'https://raidforums.com/';
    console.log(doma.match(v.white_list, "g"));
    console.log(v.Visited);
    console.log(v.toVisit);
});

chrome.runtime.onMessage.addListener(function (message, sender, sendResponse){
    if(message.msg == "domain_blacklisted"){
        document.querySelector('#show').innerHTML = "Domain is Black Listed";
            setTimeout(() => {
                document.querySelector('#show').innerHTML = "";
            }, 3000);
    }
})