var sto = chrome.storage.local;
var new_msg = chrome.runtime;
var win_loc = window.location.href;

sto.get(['white_list','black_list','toVisit','Visited'], (res)=>{
    if(res.white_list == ""){
        if(res.black_list == ""){
            new_msg.sendMessage({exe: "content_script"});
        } else {
            if(win_loc.match(res.black_list, "g") === null){
                new_msg.sendMessage({exe: "content_script"});
            } else {
                new_msg.sendMessage({exe: "no_script"});
            }
        }
    } else {
        if(win_loc.match(res.white_list, "g") !== null){
            new_msg.sendMessage({exe: "content_script"}); 
        } else {
            new_msg.sendMessage({exe: "no_script"});
        }
    }
  })